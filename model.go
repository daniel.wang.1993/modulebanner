package modulechannel

import (
	NetputeGameCore "gitlab.com/server-framework1/gamecore"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"log"
	"time"
)

var colNameBanner = "Banner"

func getBannerCol() *mongo.Collection {
	server := NetputeGameCore.GetServer()
	MongoClient := server.GetDocDb().GetClient()
	return MongoClient.Database(server.CodeName).Collection(colNameBanner)
}


func getBannerList(result *[]Banner)error{
	var err error
	var cur *mongo.Cursor
	var getChannelTrans = &NetputeGameCore.Transaction{}
	getChannelTrans.Try = func(){
		filter := bson.M{
			"start_time": bson.M{
				"$lte": time.Now().Unix(), //小於等於
			},
			"end_time": bson.M{
				"$gte": time.Now().Unix(), //小於等於
			},
		}
		if cur, err = getBannerCol().Find(nil, filter); err!= nil{
			panic(err)
		}
		defer cur.Close(nil)
		for cur.Next(nil) {
			var b Banner
			err = cur.Decode(&b)
			if err != nil {
				log.Fatal(err)
			}
			*result = append(*result,b)
		}
		err = nil
	}
	getChannelTrans.Catch = func(msg interface{}){
		err = msg.(error)
	}
	getChannelTrans.Do()
	return err
}
