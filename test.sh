GOCACHE=OFF
GO111MODULE=on
GOPROXY=direct
GOSUMDB=off
git config --global --unset-all url."git@gitlab.com:".insteadOf
git config --global --add url."git@gitlab.com:".insteadOf "https://gitlab.com/"
go env -w GOPRIVATE=gitlab.com/server-framework1/gamecore
go get gitlab.com/server-framework1/gamecore@v0.0.21
go env -w GOPRIVATE=gitlab.com/server-framework1/modulewebmember
go get gitlab.com/server-framework1/modulewebmember@v0.0.17
