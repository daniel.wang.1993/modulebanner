module gitlab.com/server-framework1/modulechannel

go 1.15

require (
	gitlab.com/server-framework1/gamecore v0.0.21
	go.mongodb.org/mongo-driver v1.5.0
)
