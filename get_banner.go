package modulechannel

import (
	NetputeGameCore "gitlab.com/server-framework1/gamecore"
)

func (sys *BannerSystem)getBanner( _ string, data NetputeGameCore.IGameRequest)interface{}{
	var banner []Banner
	var err error
	Trans := &NetputeGameCore.Transaction{}
	replyData := &NetputeGameCore.ReplyData{}
	Trans.Try = func() {
		// 取得基本資訊
		if err = getBannerList(&banner); err != nil{
			panic(1)
		}
		replyData.Code = 0
		replyData.Msg = banner
	}

	Trans.Catch = func(msg interface{}){
		replyData.Code = msg.(int)
		replyData.Msg = "fail"
	}
	Trans.Do()
	return replyData
}