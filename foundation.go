package modulechannel

import (
	"gitlab.com/server-framework1/gamecore"
)

type BannerSystem struct{
	NetputeGameCore.GameSystem
}

const (
	CmdId = "banner"
	CmdBannerList = "get_list"
)

func New() *BannerSystem {
	sys := new(BannerSystem)
	sys.RegisterDirect(CmdBannerList, sys.getBanner)
	return sys
}




