package modulechannel

import "go.mongodb.org/mongo-driver/bson/primitive"

type BannerItem struct {
	Lang    	string		`bson:"lang" json:"lang"`
	Image   	string		`bson:"image" json:"image"`
	Url     	string 		`bson:"url" json:"url"`
	Description	string		`bson:"description" json:"description"`
}

type Banner struct {
	ID			primitive.ObjectID 	`bson:"_id" json:"id,omitempty"`
	Sort 		int64			`bson:"sort" json:"sort"`
	CreateTime 	int64 			`bson:"create_time" json:"create_time"`
	StartTime 	int64			`bson:"start_time" json:"start_time"`
	EndTime 	int64			`bson:"end_time" json:"end_time"`
	Enable		bool			`bson:"enable" json:"enable"`
	Data 		[]BannerItem	`bson:"data" json:"data"`
}